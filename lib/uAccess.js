/*! uAccess 1.7.2
 *  Copyright (c) 2014 Dominik Marczuk
 *  Licence: BSD-3-Clause: http://opensource.org/licenses/BSD-3-Clause
 */

/**
 * uAccess main object
 *
 * @type {Object}
 */
var uAccess = {};

/**
 * Library version
 *
 * @type {String}
 */
uAccess.VERSION = "1.7.2";

/**
 * Permissions management
 *
 * @constructor
 */
uAccess.Permissions = function() {
	/**
	 * Permissions
	 * @type {Object}
	 */
	this.permissions = {};
};

/**
 * Grant a permission
 *
 * @param   {String}              permission Permission name
 * @returns {uAccess.Permissions}            Provides a fluent interface
 */
uAccess.Permissions.prototype.grant = function(permission)
{
	this.permissions[permission] = true;

	return this;
};

/**
 * Deny a permission
 *
 * @param   {String}              permission Permission name
 * @returns {uAccess.Permissions}            Provides a fluent interface
 */
uAccess.Permissions.prototype.deny = function(permission)
{
	this.permissions[permission] = false;

	return this;
};

/**
 * Set the entire permissions object
 *
 * @param   {Object}              permissions Permissions object
 * @returns {uAccess.Permissions}             Provides a fluent interface
 */
uAccess.Permissions.prototype.set = function(permissions)
{
	this.permissions = permissions;

	return this;
};

/**
 * Check if a given permission is defined (regardless whether it's granted or denied)
 *
 * @param   {String}  permission Permission name
 * @returns {Boolean}
 */
uAccess.Permissions.prototype.has = function(permission)
{
	return undefined !== this.permissions[permission];
};

/**
 * Get a permission's value
 *
 * @param   {String}            permission Permission name
 * @returns {Boolean|undefined}
 */
uAccess.Permissions.prototype.get = function(permission)
{
	return this.permissions[permission];
};

/**
 * A role
 *
 * @param       {String}              name        Role name
 * @param       {uAccess.Permissions} permissions An instance of a Permissions object
 * @constructor
 */
uAccess.Role = function(name, permissions)
{
	/**
	 * Superior roles
	 * @type {Object}
	 */
	this.superiors = {};

	/**
	 * Subordinate roles
	 * @type {Object}
	 */
	this.subordinates = {};

	/**
	 * Role permissions
	 * @type {uAccess.Permissions}
	 */
	this.permissions = permissions;

	/**
	 * Role name
	 * @type {String}
	 */
	this.name = name;
};

/**
 * Check if the role or any of its subordinate roles has a given permission.
 *
 * @param   {String}  permission Permission name
 * @returns {Boolean}
 */
uAccess.Role.prototype.isGranted = function(permission)
{
	var requester = arguments[1],
		result;

	if (this === requester) {
		throw new RangeError("Circular role dependency detected.");
	}

	if (typeof requester === "undefined") {
		requester = this;
	}

	if (this.permissions.has(permission)) {
		result = this.permissions.get(permission);
	} else {
		result = false;

		for (var key in this.subordinates) {
			if (this.subordinates.hasOwnProperty(key)) {
				result = result || this.subordinates[key].isGranted(permission, requester);
			}
		}
	}

	return result;
};

/**
 * Add a superior role
 *
 * @param   {uAccess.Role} superior Superior role
 * @returns {uAccess.Role}          Provides a fluent interface
 */
uAccess.Role.prototype.addSuperior = function(superior)
{
	if (typeof this.superiors[superior.name] === "undefined") {
		this.superiors[superior.name] = superior;
		superior.addSubordinate(this);
	}

	return this;
};

/**
 * Add a subordinate role
 *
 * @param   {uAccess.Role} subordinate Subordinate role
 * @returns {uAccess.Role}             Provides a fluent interface
 */
uAccess.Role.prototype.addSubordinate = function(subordinate)
{
	if (typeof this.subordinates[subordinate.name] === "undefined") {
		this.subordinates[subordinate.name] = subordinate;
		subordinate.addSuperior(this);
	}

	return this;
};

/**
 * Check if the role has a given subordinate (if the role inherits from a subordinate)
 *
 * @param  {String|uAccess.Role} role   The role that will be sought
 * @param  {Boolean}             direct Whether only direct subordinates should be checked
 * @return {Boolean}
 */
uAccess.Role.prototype.hasSubordinate = function(role, direct)
{
	var i;
	direct = !!direct;
	role = (role instanceof uAccess.Role ? role.name : role);

	if (this.subordinates[role]) {
		return true;
	}

	if (!direct) {
		for(i in this.subordinates) {
			if (this.subordinates.hasOwnProperty(i) && this.subordinates[i].hasSubordinate(role, false)) {
				return true;
			}
		}
	}

	return false;
};

/**
 * Check if the role has a given superior (if the role is inherited by the superior)
 *
 * @param  {String|uAccess.Role} role   The role that will be sought
 * @param  {Boolean}             direct Whether only direct superiors should be checked
 * @return {Boolean}
 */
uAccess.Role.prototype.hasSuperior = function(role, direct)
{
	var i;
	direct = !!direct;
	role = (role instanceof uAccess.Role ? role.name : role);

	if (this.superiors[role]) {
		return true;
	}

	if (!direct) {
		for(i in this.superiors) {
			if (this.superiors.hasOwnProperty(i) && this.superiors[i].hasSuperior(role, false)) {
				return true;
			}
		}
	}

	return false;
};

/**
 * Populate the role with data
 *
 * @param   {Object}       input Data object, as returned by uAccess.Role.serialise.
 * @param   {uAccess.Rbac} rbac  The Rbac object to retrieve the subordinate roles from
 * @returns {uAccess.Role}       Provides a fluent interface
 */
uAccess.Role.prototype.hydrate = function(input, rbac)
{
	var i,
	    j;

	// add permissions
	this.permissions.set(input.permissions);

	// add subordinate roles
	for (i = 0, j = input.subordinates.length; i < j; ++i) {
		rbac.addRole(input.subordinates[i]);
		this.addSubordinate(rbac.getRole(input.subordinates[i]));
	}

	return this;
};

/**
 * Return a plain object representation of the role
 *
 * @returns {Object} Serialised data object with the following structure:
 *                   {
 *                       permissions: {
 *                           {permissionName}: Boolean,
 *                           ...
 *                       },
 *                       subordinates: [
 *                           "{roleName}",
 *                           ...
 *                       ]
 *                   }
 */
uAccess.Role.prototype.serialise = function()
{
	var output = {
		permissions: this.permissions.permissions,
		subordinates: []
	};

	for (var key in this.subordinates) {
		if (this.subordinates.hasOwnProperty(key)) {
			output.subordinates.push(this.subordinates[key].name);
		}
	}

	return output;
};

/**
 * Client-side role-based access control implementation
 * @constructor
 */
uAccess.Rbac = function() {
	/**
	 * Defined roles
	 * @type {Object}
	 */
	this.roles = {};
};

/**
 * Add a new role
 *
 * @param  {uAccess.Role|String}        role      The role or role name
 * @param  {Boolean}                    overwrite [optional] Flag indicating whether duplicated role names should
 *                                                result in replacing the existent role with a new one. Defaults to
 *                                                false.
 * @returns {uAccess.Rbac}                        Provides a fluent interface
 */
uAccess.Rbac.prototype.addRole = function(role, overwrite)
{
	// get role name
	var roleName = (role instanceof uAccess.Role) ? role.name : role;

	// if role overwriting is diabled, check for overwrite
	if (!overwrite && this.hasRole(roleName)) {
		return this;
	}

	if (role instanceof uAccess.Role) {
		this.roles[roleName] = role;
	} else {
		this.roles[roleName] = new uAccess.Role(roleName, new uAccess.Permissions());
	}

	return this;
};

/**
 * Check if a role exists
 *
 * @param   {String}  role Name of the role or a Role object
 * @returns {Boolean}
 */
uAccess.Rbac.prototype.hasRole = function(role)
{
	if (typeof role === "string") {
		return (this.roles[role] instanceof uAccess.Role);
	} else if (role instanceof uAccess.Role) {
		var i;
		for (i in this.roles) {
			if (this.roles.hasOwnProperty(i)) {
				if (this.roles[i] === role) {
					return true;
				}
			}
		}
	} else {
		throw new Error("uAccess.Rbac#hasRole expects a string or a Role as parametre.");
	}

	return false;
};

/**
 * Fetch a role by its name.
 *
 * @param   {String}       roleName Name of the role to fetch
 * @returns {uAccess.Role}
 */
uAccess.Rbac.prototype.getRole = function(roleName)
{
	if (this.hasRole(roleName)) {
		return this.roles[roleName];
	}

	throw new ReferenceError("Role " + roleName + " has not been created.");
};

/**
 * Hydrate the Rbac with complete roles and permissions from an input array (e.g. cached)
 *
 * @param   {Object}              input Object containing roles with their subordinate roles and permissions,
 *                                      following the exact format of the uAccess.Rbac.serialise() method.
 * @return  {uAccess.Rbac}              Provides a fluent interface
 */
uAccess.Rbac.prototype.hydrate = function(input)
{
	for (var key in input) {
		if (input.hasOwnProperty(key)) {
			this.addRole(key);
			this.getRole(key).hydrate(input[key], this);
		}
	}

	return this;
};

/**
 * Serialise the contents of the Rbac to a plain object
 *
 * @returns {Object} Plain object with the following structure:
 *                   {
 *                       "{roleName}": serialised role,
 *                       ...
 *                   }
 *                   The serialised role is the output of the uAccess.Role.serialise() method.
 */
uAccess.Rbac.prototype.serialise = function()
{
	var output = {};

	for (var key in this.roles) {
		if (this.roles.hasOwnProperty(key)) {
			output[this.roles[key].name] = this.roles[key].serialise();
		}
	}

	return output;
};

/**
 * An identity
 *
 * @param       {uAccess.Permissions} permissions An instance of a Permissions object
 * @constructor
 */
uAccess.Identity = function(permissions)
{
	/**
	 * Identity permissions
	 * @type {uAccess.Permissions}
	 */
	this.permissions = permissions;

	/**
	 * Roles assigned to the identity
	 * @type {Array}
	 */
	this.roles = [];
};

/**
 * Assign a new role to the identity
 *
 * @param   {uAccess.Role}     role Role name
 * @returns {uAccess.Identity}      Provides a fluent interface
 */
uAccess.Identity.prototype.addRole = function(role)
{
	if (!(role instanceof uAccess.Role)) {
		throw new TypeError("uAccess.Identity#addRole expects to receive " +
			"an instance of uAccess.Role.");
	}

	if (this.roles.indexOf(role) === -1) {
		this.roles.push(role);
	}

	return this;
};

/**
 * Set all roles assigned to the identity
 *
 * @param   {Array}            newRoles Roles
 * @returns {uAccess.Identity}          Provides a fluent interface
 */
uAccess.Identity.prototype.setRoles = function(newRoles)
{
	var i,
	    j;

	this.roles = [];
	for (i = 0, j = newRoles.length; i < j; ++i) {
		this.addRole(newRoles[i]);
	}

	return this;
};

/**
 * Check if the identity has a given role
 *
 * @param   {String|uAccess.Role} role Role object or role name
 * @returns {Boolean}
 */
uAccess.Identity.prototype.hasRole = function(role)
{
	var i,
	    j;

	if (typeof role === "string") {
		for (i = 0, j = this.roles.length; i < j; ++i) {
			if (this.roles[i].name === role) {
				return true;
			}
		}
	} else if (role instanceof uAccess.Role) {
		return this.roles.indexOf(role) > -1;
	} else {
		throw new Error("uAccess.Identity#hasRole expects a string or a Role as parametre.");
	}

	return false;
};

/**
 * Check if the identity is granted a permission
 *
 * @param   {String}  permission Permission name
 * @returns {Boolean}
 */
uAccess.Identity.prototype.isGranted = function(permission)
{
	var i,
	    j;

	// ACL
	if (this.permissions.has(permission)) {
		return this.permissions.get(permission);
	}

	// RBAC
	for (i = 0, j = this.roles.length; i < j; ++i) {
		if (this.roles[i].isGranted(permission)) {
			return true;
		}
	}

	return false;
};

/**
 * Populate the identity with data
 *
 * @param   {Object}           input Data object as returned by uAccess.Identity.serialise.
 * @param   {uAccess.Rbac}     rbac  The Rbac object to retrieve roles from
 * @returns {uAccess.Identity}       Provides a fluent interface
 */
uAccess.Identity.prototype.hydrate = function(input, rbac)
{
	var i,
	    j;

	// add ACL permissions
	this.permissions.set(input.permissions);

	for (i = 0, j = input.roles.length; i < j; ++i) {
		rbac.addRole(input.roles[i]);
		this.addRole(rbac.getRole(input.roles[i]));
	}

	return this;
};

/**
 * Return a plain object representation of the identity
 *
 * @returns {Object} Object with the followinf structure:
 *                   {
 *                       permissions: {
 *                           {String} permissionName: {Boolean},
 *                           ...
 *                       ),
 *                       roles: [
 *                           {String} roleName,
 *                           ...
 *                       ]
 *                   }
 */
uAccess.Identity.prototype.serialise = function()
{
	var i,
	    j,
	    output = {
	        permissions: this.permissions.permissions,
	        roles: []
	    };

	for (i = 0, j = this.roles.length; i < j; ++i) {
		output.roles.push(this.roles[i].name);
	}

	return output;
};

/**
 * uAccess object factory
 *
 * @constructor
 */
uAccess.Factory = function() {};

/**
 * Construct an Identity object
 *
 * @returns {uAccess.Identity}
 */
uAccess.Factory.prototype.identity = function() {
	return new uAccess.Identity(this.permissions());
};

/**
 * Construct a Permissions object
 *
 * @returns {uAccess.Permissions}
 */
uAccess.Factory.prototype.permissions = function() {
	return new uAccess.Permissions();
};

/**
 * Construct a Rbac object
 *
 * @returns {uAccess.Rbac}
 */
uAccess.Factory.prototype.rbac = function() {
	return new uAccess.Rbac();
};

/**
 * Construct a Role object
 *
 * @returns {uAccess.Role}
 */
uAccess.Factory.prototype.role = function(roleName) {
	return new uAccess.Role(roleName, this.permissions());
};

module.exports = uAccess;
