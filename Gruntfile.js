module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON("package.json"),
		copyright: "/*! uAccess <%= pkg.version %>\n" +
			" *  Copyright (c) 2014 Dominik Marczuk\n" +
			" *  Licence: BSD-3-Clause: http://opensource.org/licenses/BSD-3-Clause\n" +
			" */\n",
		concat: {
			options: {
				banner: "<%= copyright %>",
				process: function(src, path) {
					var result = grunt.template.process(src);
					if (path.indexOf(".browser") === -1 && path.indexOf(".node") === -1) {
						result = result.replace(/\s*['"]use strict['"];\n/g, "");
					}
					return result;
				}
			},
			dev: {
				files: {
					"dist/uAccess.js": [
						"src/header.browser",
						"src/uAccess.js",
						"src/Permissions.js",
						"src/Role.js",
						"src/Rbac.js",
						"src/Identity.js",
						"src/Factory.js",
						"src/footer.browser"
					]
				}
			},
			node: {
				files: {
					"lib/uAccess.js": [
						"src/header.node",
						"src/uAccess.js",
						"src/Permissions.js",
						"src/Role.js",
						"src/Rbac.js",
						"src/Identity.js",
						"src/Factory.js",
						"src/footer.node"
					]
				}
			}
		},
		uglify: {
			dist: {
				options: {
					mangle: true,
					compress: {
						drop_console: true
					},
					banner: "<%= copyright %>"
				},
				files: [
					{ src: "dist/uAccess.js", dest: "dist/uAccess.min.js" }
				]
			}
		},
		jshint: {
			test: {
				options: {
					browser: true,
					camelcase: true,
					curly: true,
					eqeqeq: true,
					es3: true,
					forin: true,
					freeze: true,
					immed: true,
					indent: 4,
					latedef: true,
					maxlen: 120,
					newcap: true,
					noarg: true,
					noempty: true,
					nomen: true,
					nonbsp: false,
					nonew: true,
					plusplus: false,
					quotmark: true,
					undef: true,
					strict: false,
					trailing: true,
					unused: true,
					globals: {
						Mingos: true,
						uAccess: true
					}
				},
				files: {
					src: ["src/**/*.js"]
				}
			}
		},
		karma: {
			test: {
				configFile: "karma.conf.js",
				singleRun: true,
				reporters: ["dots", "coverage"]
			}
		},
		watch: {
			dev: {
				files: [
					"src/**/*.js",
					"tests/**/*.js"
				],
				tasks: [
					"test"
				]
			}
		}
	});

	// Load tasks
	grunt.loadNpmTasks("grunt-contrib-concat");
	grunt.loadNpmTasks("grunt-contrib-jshint");
	grunt.loadNpmTasks("grunt-contrib-uglify");
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.loadNpmTasks("grunt-karma");
	grunt.loadNpmTasks("grunt-replace");

	// Custom tasks
	grunt.registerTask("update_bower", "Update bower.json data from package.json", function (key, value) {
		var packageJson = grunt.file.readJSON("package.json"),
			bowerJson = grunt.file.readJSON("bower.json");

		bowerJson.name = packageJson.name;
		bowerJson.description = packageJson.description;
		bowerJson.version = packageJson.version;
		bowerJson.authors = [packageJson.author];

		grunt.file.write("bower.json", JSON.stringify(bowerJson, null, 2));
	});
	grunt.registerTask("default", ["concat"]);
	grunt.registerTask("dist", [
		"concat",
		"uglify",
		"update_bower"
	]);
	grunt.registerTask("test", [
		"jshint",
		"concat",
		"karma"
	]);
};
