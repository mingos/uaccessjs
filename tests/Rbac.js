describe("Rbac", function() {
	"use strict";

	var rbac;

	/**
	 * @type {Mingos.uAccess.Factory}
	 */
	var factory;

	beforeEach(function() {
		factory = new Mingos.uAccess.Factory();
	});

	beforeEach(function() {
		rbac = factory.rbac();
	});

	it("adds roles", function() {
		rbac.addRole("sir_robin");
		rbac.addRole(factory.role("sir_lancelot"));

		expect(typeof rbac.getRole("sir_robin")).toBe("object");
		expect(typeof rbac.getRole("sir_lancelot")).toBe("object");
		expect(rbac.getRole("sir_robin") instanceof Mingos.uAccess.Role).toBeTruthy();
		expect(rbac.getRole("sir_lancelot") instanceof Mingos.uAccess.Role).toBeTruthy();
	});

	it("overwrites roles if asked to", function() {
		var role = factory.role("sir_robin");
		rbac.addRole(role);

		// new role, but the name clashes
		rbac.addRole("sir_robin");

		expect(rbac.getRole("sir_robin")).toBe(role);

		// new role, force overwrite
		rbac.addRole("sir_robin", true);

		expect(rbac.getRole("sir_robin")).not.toBe(role);
	});

	it("checks role existence", function() {
		var arthur = factory.role("king_arthur"),
		    lancelot = factory.role("sir_lancelot");

		rbac.addRole(lancelot);

		expect(rbac.hasRole("king_arthur")).toBeFalsy();
		expect(rbac.hasRole(arthur)).toBeFalsy();
		expect(rbac.hasRole("sir_lancelot")).toBeTruthy();
		expect(rbac.hasRole(lancelot)).toBeTruthy();
	});

	it("fetches roles", function() {
		var role = factory.role("king_arthur");
		rbac.addRole(role);

		expect(rbac.getRole("king_arthur")).toBe(role);
		expect(function() {
			rbac.getRole("bridgekeeper");
		}).toThrow("Role bridgekeeper has not been created.");
	});

	it("is serialisable", function() {
		rbac.addRole("king_arthur");
		rbac.addRole("sir_robin");
		rbac.addRole("minstrel");

		var minstrel = rbac.getRole("minstrel");

		spyOn(minstrel, "serialise").andCallThrough();

		var serialised = rbac.serialise();

		expect(serialised.king_arthur).toBeDefined();
		expect(serialised.sir_robin).toBeDefined();
		expect(serialised.minstrel).toBeDefined();
		expect(minstrel.serialise).toHaveBeenCalled();
	});

	it("is hydratable", function() {
		rbac.hydrate({
			king_arthur: {
				permissions: {},
				subordinates: []
			},
			"sir_robin": {
				permissions: {},
				subordinates: []
			}
		});

		expect(rbac.hasRole("king_arthur")).toBeTruthy();
		expect(rbac.hasRole("sir_robin")).toBeTruthy();
	});
});
