describe("Role", function() {
	"use strict";

	/**
	 * @type {Mingos.uAccess.Factory}
	 */
	var factory;

	beforeEach(function() {
		factory = new Mingos.uAccess.Factory();
	});

	it("allows fetching the role name)", function() {
		var role = factory.role("sir_robin");
		expect(role.name).toBe("sir_robin");
	});

	it("sets subordinates/superiors", function() {
		var arthur = factory.role("king_arthur");
		var robin = factory.role("sir_robin");
		var minstrel = factory.role("minstrel");

		arthur.addSubordinate(robin);
		minstrel.addSuperior(robin);

		minstrel.permissions.grant("sing");
		robin.permissions.grant("run_away");
		robin.permissions.deny("sing");
		arthur.permissions.grant("lob_hand_grenade");

		expect(arthur.isGranted("sing")).toBeFalsy();
		expect(arthur.isGranted("run_away")).toBeTruthy();
		expect(arthur.isGranted("lob_hand_grenade")).toBeTruthy();

		expect(robin.isGranted("sing")).toBeFalsy();
		expect(robin.isGranted("run_away")).toBeTruthy();
		expect(robin.isGranted("lob_hand_grenade")).toBeFalsy();

		expect(minstrel.isGranted("sing")).toBeTruthy();
		expect(minstrel.isGranted("run_away")).toBeFalsy();
		expect(minstrel.isGranted("lob_hand_grenade")).toBeFalsy();
	});

	it("should disallow circular dependencies between subordinates/superiors", function() {
		var rock = factory.role("rock");
		var paper = factory.role("paper");
		var scissors = factory.role("scissors");

		rock.addSubordinate(scissors);
		scissors.addSubordinate(paper);
		paper.addSubordinate(rock);

		expect(function() {
			rock.isGranted("win");
		}).toThrow("Circular role dependency detected.");
	});

	it("should allow serialisation", function() {
		var role = factory.role("king_arthur");
		var subordinate = factory.role("sir_robin");

		role
			.addSubordinate(subordinate);
		role.permissions
			.grant("lob_hand_grenade")
			.deny("chicken_out");

		var expected = {
			permissions: {
				lob_hand_grenade: true,
				chicken_out: false
			},
			subordinates: [
				"sir_robin"
			]
		};

		expect(JSON.stringify(role.serialise())).toBe(JSON.stringify(expected));
	});

	it("should be hydratable", function() {
		var rbac = factory.rbac();
		var role = factory.role("king_arthur");
		rbac.addRole(role);

		role.hydrate({
			permissions: {
				lob_hand_grenade: true,
				chicken_out: false
			},
			subordinates: [
				"sir_robin"
			]
		}, rbac);

		expect(role.isGranted("lob_hand_grenade")).toBeTruthy();

		// give Sir Robin a permission and check whether King Arthur receives it
		rbac.getRole("sir_robin").permissions.grant("confront_the_bridgekeeper");
		expect(role.isGranted("confront_the_bridgekeeper")).toBeTruthy();
	});

	it("should check for direct and indirect subordinates", function() {
		var grandfather = factory.role("grandfather");
		var father = factory.role("father");
		var uncle = factory.role("uncle");
		var son = factory.role("son");

		grandfather
			.addSubordinate(father)
			.addSubordinate(uncle);

		father
			.addSubordinate(son);

		expect(grandfather.hasSubordinate(father)).toBeTruthy();
		expect(grandfather.hasSubordinate(father, true)).toBeTruthy();
		expect(grandfather.hasSubordinate(uncle)).toBeTruthy();
		expect(grandfather.hasSubordinate(uncle, true)).toBeTruthy();
		expect(grandfather.hasSubordinate(son)).toBeTruthy();
		expect(grandfather.hasSubordinate(son, true)).toBeFalsy();

		expect(father.hasSubordinate(grandfather)).toBeFalsy();
		expect(father.hasSubordinate(grandfather, true)).toBeFalsy();
		expect(father.hasSubordinate(uncle)).toBeFalsy();
		expect(father.hasSubordinate(uncle), true).toBeFalsy();
		expect(father.hasSubordinate(son)).toBeTruthy();
		expect(father.hasSubordinate(son), true).toBeTruthy();

		expect(son.hasSubordinate(grandfather)).toBeFalsy();
		expect(son.hasSubordinate(grandfather, true)).toBeFalsy();
		expect(son.hasSubordinate(uncle)).toBeFalsy();
		expect(son.hasSubordinate(uncle), true).toBeFalsy();
		expect(son.hasSubordinate(father)).toBeFalsy();
		expect(son.hasSubordinate(father), true).toBeFalsy();

		expect(uncle.hasSubordinate(grandfather)).toBeFalsy();
		expect(uncle.hasSubordinate(grandfather, true)).toBeFalsy();
		expect(uncle.hasSubordinate(son)).toBeFalsy();
		expect(uncle.hasSubordinate(son), true).toBeFalsy();
		expect(uncle.hasSubordinate(father)).toBeFalsy();
		expect(uncle.hasSubordinate(father), true).toBeFalsy();

		expect(grandfather.hasSubordinate("father")).toBeTruthy();
		expect(grandfather.hasSubordinate("father", true)).toBeTruthy();
		expect(grandfather.hasSubordinate("uncle")).toBeTruthy();
		expect(grandfather.hasSubordinate("uncle", true)).toBeTruthy();
		expect(grandfather.hasSubordinate("son")).toBeTruthy();
		expect(grandfather.hasSubordinate("son", true)).toBeFalsy();

		expect(father.hasSubordinate("grandfather")).toBeFalsy();
		expect(father.hasSubordinate("grandfather", true)).toBeFalsy();
		expect(father.hasSubordinate("uncle")).toBeFalsy();
		expect(father.hasSubordinate("uncle"), true).toBeFalsy();
		expect(father.hasSubordinate("son")).toBeTruthy();
		expect(father.hasSubordinate("son"), true).toBeTruthy();

		expect(son.hasSubordinate("grandfather")).toBeFalsy();
		expect(son.hasSubordinate("grandfather", true)).toBeFalsy();
		expect(son.hasSubordinate("uncle")).toBeFalsy();
		expect(son.hasSubordinate("uncle"), true).toBeFalsy();
		expect(son.hasSubordinate("father")).toBeFalsy();
		expect(son.hasSubordinate("father"), true).toBeFalsy();

		expect(uncle.hasSubordinate("grandfather")).toBeFalsy();
		expect(uncle.hasSubordinate("grandfather", true)).toBeFalsy();
		expect(uncle.hasSubordinate("son")).toBeFalsy();
		expect(uncle.hasSubordinate("son"), true).toBeFalsy();
		expect(uncle.hasSubordinate("father")).toBeFalsy();
		expect(uncle.hasSubordinate("father"), true).toBeFalsy();
	});

	it("should check for direct and indirect superiors", function() {
		var grandfather = factory.role("grandfather");
		var father = factory.role("father");
		var uncle = factory.role("uncle");
		var son = factory.role("son");

		grandfather
			.addSubordinate(father)
			.addSubordinate(uncle);

		father
			.addSubordinate(son);

		expect(grandfather.hasSuperior(father)).toBeFalsy();
		expect(grandfather.hasSuperior(father, true)).toBeFalsy();
		expect(grandfather.hasSuperior(uncle)).toBeFalsy();
		expect(grandfather.hasSuperior(uncle, true)).toBeFalsy();
		expect(grandfather.hasSuperior(son)).toBeFalsy();
		expect(grandfather.hasSuperior(son, true)).toBeFalsy();

		expect(father.hasSuperior(grandfather)).toBeTruthy();
		expect(father.hasSuperior(grandfather, true)).toBeTruthy();
		expect(father.hasSuperior(uncle)).toBeFalsy();
		expect(father.hasSuperior(uncle), true).toBeFalsy();
		expect(father.hasSuperior(son)).toBeFalsy();
		expect(father.hasSuperior(son), true).toBeFalsy();

		expect(son.hasSuperior(grandfather)).toBeTruthy();
		expect(son.hasSuperior(grandfather, true)).toBeFalsy();
		expect(son.hasSuperior(uncle)).toBeFalsy();
		expect(son.hasSuperior(uncle), true).toBeFalsy();
		expect(son.hasSuperior(father)).toBeTruthy();
		expect(son.hasSuperior(father), true).toBeTruthy();

		expect(uncle.hasSuperior(grandfather)).toBeTruthy();
		expect(uncle.hasSuperior(grandfather, true)).toBeTruthy();
		expect(uncle.hasSuperior(son)).toBeFalsy();
		expect(uncle.hasSuperior(son), true).toBeFalsy();
		expect(uncle.hasSuperior(father)).toBeFalsy();
		expect(uncle.hasSuperior(father), true).toBeFalsy();

		expect(grandfather.hasSuperior("father")).toBeFalsy();
		expect(grandfather.hasSuperior("father", true)).toBeFalsy();
		expect(grandfather.hasSuperior("uncle")).toBeFalsy();
		expect(grandfather.hasSuperior("uncle", true)).toBeFalsy();
		expect(grandfather.hasSuperior("son")).toBeFalsy();
		expect(grandfather.hasSuperior("son", true)).toBeFalsy();

		expect(father.hasSuperior("grandfather")).toBeTruthy();
		expect(father.hasSuperior("grandfather", true)).toBeTruthy();
		expect(father.hasSuperior("uncle")).toBeFalsy();
		expect(father.hasSuperior("uncle"), true).toBeFalsy();
		expect(father.hasSuperior("son")).toBeFalsy();
		expect(father.hasSuperior("son"), true).toBeFalsy();

		expect(son.hasSuperior("grandfather")).toBeTruthy();
		expect(son.hasSuperior("grandfather", true)).toBeFalsy();
		expect(son.hasSuperior("uncle")).toBeFalsy();
		expect(son.hasSuperior("uncle"), true).toBeFalsy();
		expect(son.hasSuperior("father")).toBeTruthy();
		expect(son.hasSuperior("father"), true).toBeTruthy();

		expect(uncle.hasSuperior("grandfather")).toBeTruthy();
		expect(uncle.hasSuperior("grandfather", true)).toBeTruthy();
		expect(uncle.hasSuperior("son")).toBeFalsy();
		expect(uncle.hasSuperior("son"), true).toBeFalsy();
		expect(uncle.hasSuperior("father")).toBeFalsy();
		expect(uncle.hasSuperior("father"), true).toBeFalsy();
	});
});
