describe("Factory", function() {
	"use strict";

	var factory;

	beforeEach(function() {
		factory = new Mingos.uAccess.Factory();
	});

	it("constructs Identity objects", function() {
		expect(factory.identity() instanceof Mingos.uAccess.Identity).toBeTruthy();
	});

	it("constructs Permissions objects", function() {
		expect(factory.permissions() instanceof Mingos.uAccess.Permissions).toBeTruthy();
	});

	it("constructs Rbac objects", function() {
		expect(factory.rbac() instanceof Mingos.uAccess.Rbac).toBeTruthy();
	});

	it("constructs Role objects", function() {
		expect(factory.role("sir lancelot") instanceof Mingos.uAccess.Role).toBeTruthy();
	});
});
