describe("Permissions", function() {
	"use strict";

	/**
	 * @type {Mingos.uAccess.Factory}
	 */
	var factory;

	beforeEach(function() {
		factory = new Mingos.uAccess.Factory();
	});

	it("allows granting/denying permissions", function() {
		var role = factory.role("sir_robin");
		role.permissions
			.grant("run_away")
			.grant("chicken_out")
			.deny("fight_the_three_headed_knight");

		expect(role.isGranted("run_away")).toBeTruthy();
		expect(role.isGranted("chicken_out")).toBeTruthy();
		expect(role.isGranted("fight_the_three_headed_knight")).toBeFalsy();
	});

	it("should correctly indicate permission ownership", function() {
		var role = factory.role("sir_robin");

		role.permissions
			.grant("run_away")
			.deny("fight_the_three_headed_knight");

		expect(role.permissions.has("run_away")).toBeTruthy();
		expect(role.permissions.has("fight_the_three_headed_knight")).toBeTruthy();
		expect(role.permissions.has("find_the_holy_grail")).toBeFalsy();
	});
});
