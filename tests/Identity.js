describe("Identity", function() {
	"use strict";

	/**
	 * @type {Mingos.uAccess.Identity}
	 */
	var identity;

	/**
	 * @type {Mingos.uAccess.Rbac}
	 */
	var rbac;

	/**
	 * @type {Mingos.uAccess.Factory}
	 */
	var factory;

	beforeEach(function() {
		factory = new Mingos.uAccess.Factory();
	});

	beforeEach(function() {
		rbac = factory.rbac();
		identity = factory.identity();

		rbac.addRole("king_arthur");
		rbac.addRole("sir_robin");
		rbac.addRole("sir_lancelot");
		rbac.addRole("sir_galahad");
		rbac.addRole("sir_bedevere");
		rbac.addRole("minstrel");

		rbac.getRole("king_arthur").permissions.grant("lob_hand_grenade");
		rbac.getRole("sir_robin").permissions.grant("run_away");
		rbac.getRole("minstrel").permissions.grant("sing");

		rbac.getRole("king_arthur")
			.addSubordinate(rbac.getRole("sir_robin"))
			.addSubordinate(rbac.getRole("sir_lancelot"))
			.addSubordinate(rbac.getRole("sir_galahad"))
			.addSubordinate(rbac.getRole("sir_bedevere"));

		rbac.getRole("sir_robin")
			.addSubordinate(rbac.getRole("minstrel"));
	});

	it("has assignable roles", function() {
		identity.addRole(rbac.getRole("sir_lancelot"));
		identity.addRole(rbac.getRole("sir_bedevere"));

		expect(identity.hasRole("sir_lancelot")).toBeTruthy();
		expect(identity.hasRole(rbac.getRole("sir_lancelot"))).toBeTruthy();
		expect(identity.hasRole("sir_bedevere")).toBeTruthy();
		expect(identity.hasRole(rbac.getRole("sir_bedevere"))).toBeTruthy();

		identity.setRoles([
			rbac.getRole("sir_galahad"),
			rbac.getRole("sir_robin")
		]);

		expect(identity.hasRole("sir_lancelot")).toBeFalsy();
		expect(identity.hasRole(rbac.getRole("sir_lancelot"))).toBeFalsy();
		expect(identity.hasRole("sir_bedevere")).toBeFalsy();
		expect(identity.hasRole(rbac.getRole("sir_bedevere"))).toBeFalsy();
		expect(identity.hasRole("sir_galahad")).toBeTruthy();
		expect(identity.hasRole(rbac.getRole("sir_galahad"))).toBeTruthy();
		expect(identity.hasRole("sir_robin")).toBeTruthy();
		expect(identity.hasRole(rbac.getRole("sir_robin"))).toBeTruthy();

		expect(function() {
			identity.addRole("three_headed_knight");
		}).toThrow("uAccess.Identity#addRole expects to receive an instance of uAccess.Role.");
	});

	it("errors when checking role existence using bad type", function() {
		expect(function() {
			identity.hasRole(1);
		}).toThrow("uAccess.Identity#hasRole expects a string or a Role as parametre.");
	});

	it("gets roles", function() {
		identity.setRoles([
			rbac.getRole("sir_galahad"),
			rbac.getRole("sir_robin")
		]);

		expect(identity.roles).toEqual([
			rbac.getRole("sir_galahad"),
			rbac.getRole("sir_robin")
		]);
	});

	it("checks roles", function() {
		identity.setRoles([
			rbac.getRole("sir_galahad"),
			rbac.getRole("sir_robin")
		]);

		expect(identity.hasRole("sir_galahad")).toBeTruthy();
		expect(identity.hasRole("sir_robin")).toBeTruthy();
		expect(identity.hasRole("sir_lancelot")).toBeFalsy();

		expect(identity.hasRole(rbac.getRole("sir_galahad"))).toBeTruthy();
		expect(identity.hasRole(rbac.getRole("sir_robin"))).toBeTruthy();
		expect(identity.hasRole(rbac.getRole("sir_lancelot"))).toBeFalsy();
	});

	it("correctly checks permissions for all roles", function() {
		identity.setRoles([
			rbac.getRole("sir_robin")
		]);

		expect(identity.isGranted("sing")).toBeTruthy();
		expect(identity.isGranted("run_away")).toBeTruthy();
		expect(identity.isGranted("lob_hand_grenade")).toBeFalsy();
		expect(identity.isGranted("find_holy_grail")).toBeFalsy();

		identity.setRoles([
			rbac.getRole("king_arthur")
		]);

		expect(identity.isGranted("sing")).toBeTruthy();
		expect(identity.isGranted("run_away")).toBeTruthy();
		expect(identity.isGranted("lob_hand_grenade")).toBeTruthy();
		expect(identity.isGranted("find_holy_grail")).toBeFalsy();

		identity.setRoles([
			rbac.getRole("sir_lancelot")
		]);

		expect(identity.isGranted("sing")).toBeFalsy();
		expect(identity.isGranted("run_away")).toBeFalsy();
		expect(identity.isGranted("lob_hand_grenade")).toBeFalsy();
		expect(identity.isGranted("find_holy_grail")).toBeFalsy();
	});

	it("checks ACL before RBAC", function() {
		identity.setRoles([
			rbac.getRole("sir_robin")
		]);

		identity.permissions.grant("lob_hand_grenade");
		identity.permissions.deny("run_away");

		expect(identity.isGranted("lob_hand_grenade")).toBeTruthy();
		expect(identity.isGranted("run_away")).toBeFalsy();
	});

	it("should be serialisable", function() {
		identity.setRoles([
			rbac.getRole("sir_robin"),
			rbac.getRole("sir_lancelot")
		]);

		identity.permissions.deny("wield_the_excalibur");
		identity.permissions.grant("find_the_holy_grail");

		expect(identity.serialise()).toEqual({
			roles: [
				"sir_robin",
				"sir_lancelot"
			],
			permissions: {
				wield_the_excalibur: false,
				find_the_holy_grail: true
			}
		});
	});

	it("should be hydratable", function() {
		var serialised = {
			roles: [
				"sir_robin",
				"sir_lancelot"
			],
			permissions: {
				wield_the_excalibur: false,
				find_the_holy_grail: true
			}
		};

		identity.hydrate(serialised, rbac);

		expect(identity.hasRole("sir_robin")).toBeTruthy();
		expect(identity.hasRole("sir_lancelot")).toBeTruthy();
		expect(identity.permissions.has("wield_the_excalibur")).toBeTruthy();
		expect(identity.permissions.has("find_the_holy_grail")).toBeTruthy();
	});
});
