/**
 * An identity
 *
 * @param       {uAccess.Permissions} permissions An instance of a Permissions object
 * @constructor
 */
uAccess.Identity = function(permissions)
{
	"use strict";

	/**
	 * Identity permissions
	 * @type {uAccess.Permissions}
	 */
	this.permissions = permissions;

	/**
	 * Roles assigned to the identity
	 * @type {Array}
	 */
	this.roles = [];
};

/**
 * Assign a new role to the identity
 *
 * @param   {uAccess.Role}     role Role name
 * @returns {uAccess.Identity}      Provides a fluent interface
 */
uAccess.Identity.prototype.addRole = function(role)
{
	"use strict";

	if (!(role instanceof uAccess.Role)) {
		throw new TypeError("uAccess.Identity#addRole expects to receive " +
			"an instance of uAccess.Role.");
	}

	if (this.roles.indexOf(role) === -1) {
		this.roles.push(role);
	}

	return this;
};

/**
 * Set all roles assigned to the identity
 *
 * @param   {Array}            newRoles Roles
 * @returns {uAccess.Identity}          Provides a fluent interface
 */
uAccess.Identity.prototype.setRoles = function(newRoles)
{
	"use strict";

	var i,
	    j;

	this.roles = [];
	for (i = 0, j = newRoles.length; i < j; ++i) {
		this.addRole(newRoles[i]);
	}

	return this;
};

/**
 * Check if the identity has a given role
 *
 * @param   {String|uAccess.Role} role Role object or role name
 * @returns {Boolean}
 */
uAccess.Identity.prototype.hasRole = function(role)
{
	"use strict";

	var i,
	    j;

	if (typeof role === "string") {
		for (i = 0, j = this.roles.length; i < j; ++i) {
			if (this.roles[i].name === role) {
				return true;
			}
		}
	} else if (role instanceof uAccess.Role) {
		return this.roles.indexOf(role) > -1;
	} else {
		throw new Error("uAccess.Identity#hasRole expects a string or a Role as parametre.");
	}

	return false;
};

/**
 * Check if the identity is granted a permission
 *
 * @param   {String}  permission Permission name
 * @returns {Boolean}
 */
uAccess.Identity.prototype.isGranted = function(permission)
{
	"use strict";

	var i,
	    j;

	// ACL
	if (this.permissions.has(permission)) {
		return this.permissions.get(permission);
	}

	// RBAC
	for (i = 0, j = this.roles.length; i < j; ++i) {
		if (this.roles[i].isGranted(permission)) {
			return true;
		}
	}

	return false;
};

/**
 * Populate the identity with data
 *
 * @param   {Object}           input Data object as returned by uAccess.Identity.serialise.
 * @param   {uAccess.Rbac}     rbac  The Rbac object to retrieve roles from
 * @returns {uAccess.Identity}       Provides a fluent interface
 */
uAccess.Identity.prototype.hydrate = function(input, rbac)
{
	"use strict";

	var i,
	    j;

	// add ACL permissions
	this.permissions.set(input.permissions);

	for (i = 0, j = input.roles.length; i < j; ++i) {
		rbac.addRole(input.roles[i]);
		this.addRole(rbac.getRole(input.roles[i]));
	}

	return this;
};

/**
 * Return a plain object representation of the identity
 *
 * @returns {Object} Object with the followinf structure:
 *                   {
 *                       permissions: {
 *                           {String} permissionName: {Boolean},
 *                           ...
 *                       ),
 *                       roles: [
 *                           {String} roleName,
 *                           ...
 *                       ]
 *                   }
 */
uAccess.Identity.prototype.serialise = function()
{
	"use strict";

	var i,
	    j,
	    output = {
	        permissions: this.permissions.permissions,
	        roles: []
	    };

	for (i = 0, j = this.roles.length; i < j; ++i) {
		output.roles.push(this.roles[i].name);
	}

	return output;
};
