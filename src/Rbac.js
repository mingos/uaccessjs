/**
 * Client-side role-based access control implementation
 * @constructor
 */
uAccess.Rbac = function() {
	"use strict";

	/**
	 * Defined roles
	 * @type {Object}
	 */
	this.roles = {};
};

/**
 * Add a new role
 *
 * @param  {uAccess.Role|String}        role      The role or role name
 * @param  {Boolean}                    overwrite [optional] Flag indicating whether duplicated role names should
 *                                                result in replacing the existent role with a new one. Defaults to
 *                                                false.
 * @returns {uAccess.Rbac}                        Provides a fluent interface
 */
uAccess.Rbac.prototype.addRole = function(role, overwrite)
{
	"use strict";

	// get role name
	var roleName = (role instanceof uAccess.Role) ? role.name : role;

	// if role overwriting is diabled, check for overwrite
	if (!overwrite && this.hasRole(roleName)) {
		return this;
	}

	if (role instanceof uAccess.Role) {
		this.roles[roleName] = role;
	} else {
		this.roles[roleName] = new uAccess.Role(roleName, new uAccess.Permissions());
	}

	return this;
};

/**
 * Check if a role exists
 *
 * @param   {String}  role Name of the role or a Role object
 * @returns {Boolean}
 */
uAccess.Rbac.prototype.hasRole = function(role)
{
	"use strict";

	if (typeof role === "string") {
		return (this.roles[role] instanceof uAccess.Role);
	} else if (role instanceof uAccess.Role) {
		var i;
		for (i in this.roles) {
			if (this.roles.hasOwnProperty(i)) {
				if (this.roles[i] === role) {
					return true;
				}
			}
		}
	} else {
		throw new Error("uAccess.Rbac#hasRole expects a string or a Role as parametre.");
	}

	return false;
};

/**
 * Fetch a role by its name.
 *
 * @param   {String}       roleName Name of the role to fetch
 * @returns {uAccess.Role}
 */
uAccess.Rbac.prototype.getRole = function(roleName)
{
	"use strict";

	if (this.hasRole(roleName)) {
		return this.roles[roleName];
	}

	throw new ReferenceError("Role " + roleName + " has not been created.");
};

/**
 * Hydrate the Rbac with complete roles and permissions from an input array (e.g. cached)
 *
 * @param   {Object}              input Object containing roles with their subordinate roles and permissions,
 *                                      following the exact format of the uAccess.Rbac.serialise() method.
 * @return  {uAccess.Rbac}              Provides a fluent interface
 */
uAccess.Rbac.prototype.hydrate = function(input)
{
	"use strict";

	for (var key in input) {
		if (input.hasOwnProperty(key)) {
			this.addRole(key);
			this.getRole(key).hydrate(input[key], this);
		}
	}

	return this;
};

/**
 * Serialise the contents of the Rbac to a plain object
 *
 * @returns {Object} Plain object with the following structure:
 *                   {
 *                       "{roleName}": serialised role,
 *                       ...
 *                   }
 *                   The serialised role is the output of the uAccess.Role.serialise() method.
 */
uAccess.Rbac.prototype.serialise = function()
{
	"use strict";

	var output = {};

	for (var key in this.roles) {
		if (this.roles.hasOwnProperty(key)) {
			output[this.roles[key].name] = this.roles[key].serialise();
		}
	}

	return output;
};
