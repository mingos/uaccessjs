/**
 * uAccess main object
 *
 * @type {Object}
 */
var uAccess = {};

/**
 * Library version
 *
 * @type {String}
 */
uAccess.VERSION = "<%= pkg.version %>";
