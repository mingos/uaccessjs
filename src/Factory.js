/**
 * uAccess object factory
 *
 * @constructor
 */
uAccess.Factory = function() {};

/**
 * Construct an Identity object
 *
 * @returns {uAccess.Identity}
 */
uAccess.Factory.prototype.identity = function() {
	"use strict";

	return new uAccess.Identity(this.permissions());
};

/**
 * Construct a Permissions object
 *
 * @returns {uAccess.Permissions}
 */
uAccess.Factory.prototype.permissions = function() {
	"use strict";

	return new uAccess.Permissions();
};

/**
 * Construct a Rbac object
 *
 * @returns {uAccess.Rbac}
 */
uAccess.Factory.prototype.rbac = function() {
	"use strict";

	return new uAccess.Rbac();
};

/**
 * Construct a Role object
 *
 * @returns {uAccess.Role}
 */
uAccess.Factory.prototype.role = function(roleName) {
	"use strict";

	return new uAccess.Role(roleName, this.permissions());
};
