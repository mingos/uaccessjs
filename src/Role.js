/**
 * A role
 *
 * @param       {String}              name        Role name
 * @param       {uAccess.Permissions} permissions An instance of a Permissions object
 * @constructor
 */
uAccess.Role = function(name, permissions)
{
	"use strict";

	/**
	 * Superior roles
	 * @type {Object}
	 */
	this.superiors = {};

	/**
	 * Subordinate roles
	 * @type {Object}
	 */
	this.subordinates = {};

	/**
	 * Role permissions
	 * @type {uAccess.Permissions}
	 */
	this.permissions = permissions;

	/**
	 * Role name
	 * @type {String}
	 */
	this.name = name;
};

/**
 * Check if the role or any of its subordinate roles has a given permission.
 *
 * @param   {String}  permission Permission name
 * @returns {Boolean}
 */
uAccess.Role.prototype.isGranted = function(permission)
{
	"use strict";

	var requester = arguments[1],
		result;

	if (this === requester) {
		throw new RangeError("Circular role dependency detected.");
	}

	if (typeof requester === "undefined") {
		requester = this;
	}

	if (this.permissions.has(permission)) {
		result = this.permissions.get(permission);
	} else {
		result = false;

		for (var key in this.subordinates) {
			if (this.subordinates.hasOwnProperty(key)) {
				result = result || this.subordinates[key].isGranted(permission, requester);
			}
		}
	}

	return result;
};

/**
 * Add a superior role
 *
 * @param   {uAccess.Role} superior Superior role
 * @returns {uAccess.Role}          Provides a fluent interface
 */
uAccess.Role.prototype.addSuperior = function(superior)
{
	"use strict";

	if (typeof this.superiors[superior.name] === "undefined") {
		this.superiors[superior.name] = superior;
		superior.addSubordinate(this);
	}

	return this;
};

/**
 * Add a subordinate role
 *
 * @param   {uAccess.Role} subordinate Subordinate role
 * @returns {uAccess.Role}             Provides a fluent interface
 */
uAccess.Role.prototype.addSubordinate = function(subordinate)
{
	"use strict";

	if (typeof this.subordinates[subordinate.name] === "undefined") {
		this.subordinates[subordinate.name] = subordinate;
		subordinate.addSuperior(this);
	}

	return this;
};

/**
 * Check if the role has a given subordinate (if the role inherits from a subordinate)
 *
 * @param  {String|uAccess.Role} role   The role that will be sought
 * @param  {Boolean}             direct Whether only direct subordinates should be checked
 * @return {Boolean}
 */
uAccess.Role.prototype.hasSubordinate = function(role, direct)
{
	"use strict";

	var i;
	direct = !!direct;
	role = (role instanceof uAccess.Role ? role.name : role);

	if (this.subordinates[role]) {
		return true;
	}

	if (!direct) {
		for(i in this.subordinates) {
			if (this.subordinates.hasOwnProperty(i) && this.subordinates[i].hasSubordinate(role, false)) {
				return true;
			}
		}
	}

	return false;
};

/**
 * Check if the role has a given superior (if the role is inherited by the superior)
 *
 * @param  {String|uAccess.Role} role   The role that will be sought
 * @param  {Boolean}             direct Whether only direct superiors should be checked
 * @return {Boolean}
 */
uAccess.Role.prototype.hasSuperior = function(role, direct)
{
	"use strict";

	var i;
	direct = !!direct;
	role = (role instanceof uAccess.Role ? role.name : role);

	if (this.superiors[role]) {
		return true;
	}

	if (!direct) {
		for(i in this.superiors) {
			if (this.superiors.hasOwnProperty(i) && this.superiors[i].hasSuperior(role, false)) {
				return true;
			}
		}
	}

	return false;
};

/**
 * Populate the role with data
 *
 * @param   {Object}       input Data object, as returned by uAccess.Role.serialise.
 * @param   {uAccess.Rbac} rbac  The Rbac object to retrieve the subordinate roles from
 * @returns {uAccess.Role}       Provides a fluent interface
 */
uAccess.Role.prototype.hydrate = function(input, rbac)
{
	"use strict";

	var i,
	    j;

	// add permissions
	this.permissions.set(input.permissions);

	// add subordinate roles
	for (i = 0, j = input.subordinates.length; i < j; ++i) {
		rbac.addRole(input.subordinates[i]);
		this.addSubordinate(rbac.getRole(input.subordinates[i]));
	}

	return this;
};

/**
 * Return a plain object representation of the role
 *
 * @returns {Object} Serialised data object with the following structure:
 *                   {
 *                       permissions: {
 *                           {permissionName}: Boolean,
 *                           ...
 *                       },
 *                       subordinates: [
 *                           "{roleName}",
 *                           ...
 *                       ]
 *                   }
 */
uAccess.Role.prototype.serialise = function()
{
	"use strict";

	var output = {
		permissions: this.permissions.permissions,
		subordinates: []
	};

	for (var key in this.subordinates) {
		if (this.subordinates.hasOwnProperty(key)) {
			output.subordinates.push(this.subordinates[key].name);
		}
	}

	return output;
};
