/**
 * Permissions management
 *
 * @constructor
 */
uAccess.Permissions = function() {
	"use strict";

	/**
	 * Permissions
	 * @type {Object}
	 */
	this.permissions = {};
};

/**
 * Grant a permission
 *
 * @param   {String}              permission Permission name
 * @returns {uAccess.Permissions}            Provides a fluent interface
 */
uAccess.Permissions.prototype.grant = function(permission)
{
	"use strict";

	this.permissions[permission] = true;

	return this;
};

/**
 * Deny a permission
 *
 * @param   {String}              permission Permission name
 * @returns {uAccess.Permissions}            Provides a fluent interface
 */
uAccess.Permissions.prototype.deny = function(permission)
{
	"use strict";

	this.permissions[permission] = false;

	return this;
};

/**
 * Set the entire permissions object
 *
 * @param   {Object}              permissions Permissions object
 * @returns {uAccess.Permissions}             Provides a fluent interface
 */
uAccess.Permissions.prototype.set = function(permissions)
{
	"use strict";

	this.permissions = permissions;

	return this;
};

/**
 * Check if a given permission is defined (regardless whether it's granted or denied)
 *
 * @param   {String}  permission Permission name
 * @returns {Boolean}
 */
uAccess.Permissions.prototype.has = function(permission)
{
	"use strict";

	return undefined !== this.permissions[permission];
};

/**
 * Get a permission's value
 *
 * @param   {String}            permission Permission name
 * @returns {Boolean|undefined}
 */
uAccess.Permissions.prototype.get = function(permission)
{
	"use strict";

	return this.permissions[permission];
};
