# uAccess

uAccess is a lightweight implementation of access control tools, supporting both role-based access control (RBAC) and access control lists (ACL).

The JavaScript implementation is designed to be usable in conjunction with its [PHP counterpart](https://bitbucket.org/mingos/uaccess).

## Factory

All objects in the uAccess tool can be created using the `Factory`.

In the browser, the uAccess object is namespaced and can be instantiated the following way:

    var uAccess = new Mingos.uAccess.Factory();

Whereas when used in the context of Node.js, it's not namespaced and is created like this:

    var ua = require("mingos-uaccess");
    var uAccess = new ua.Factory();

## RBAC

    var rbac = uAccess.rbac();

The `Rbac` object stores and fetches roles. It has no other functions.

### Creating roles

    var lumberjack = uAccess.role("lumberjack");

The role can then be added to `Rbac`:

    rbac.addRole(lumberjack);

Roles can also be created directly in `Rbac`, without instantiating them manually:

    rbac.addRole("lumberjack");

### Creating role relationships

Roles can have parent/child relationships, but unlike in a regular tree structure, a pparent can have many children AND a child can have many parents.

Roles can be added to each other via the `Role#addSubordinate` and `Role#addSuperior` functions:

    var minstrel = uAccess.role("minstrel"),
        sirRobin = uAccess.role("sir robin"),
        arthur = uAccess.role("king arthur");
    sirRobin
        .addSubordinate(minstrel)
        .addSuperior(arthur);

Once the hierarchy is set, the permissions will carry on from children to parents.

### Granting permissions

Permissions are granted (or denied) to roles using `Role.permissions#grant` and `Role.permissions#deny` functions.

    minstrel.permissions.grant("sing");
    sirRobin.permissions.deny("sing").grant("flee");
    arthur.permissions.grant("slay rabbit");

In the above example, we grant the `minstrel` a permission, but deny it to its direct superior, `sirRobin`. The denied permission is carried over to the next superior in the chain, `arthur`. Likewise, `sirRobin` is granted permission to flee and `arthur` is allowed to flee as well, though `minstrel` is not. Finally, only `arthur` is allowed to slay the vile beast rabbit of Caerbannog.

It isn't necessary to keep references to all roles in order to grant or deny permissions. If the roles are added to `Rbac`, they can be fetched by name:

    rbac.getRole("minstrel").permissions.grant("sing");

### Checking permissions

Permissions can be checked using the `Role#isGranted` function.

    minstrel.isGranted("sing"); //true
    minstrel.isGranted("flee"); //false
    minstrel.isGranted("slay rabbit"); // false

Again, it's a much better idea to use the `Rbac` object for role fetching:

    rbac.getRole("minstrel").isGranted("sing");

## ACL

    var identity = uAccess.identity();

Access control is based on an individual identity rather than a more generic role. An `Identity` object is used to store permissions in the same way a `Role` is, but there are no subordinate/superior roles or identities to check when determining if access to a permission is granted or denied.

### Granting permissions

Granting and denying permissions is done via the `Identity.permissions#grant` and `Identity.permissions#deny` functions.

    identity.permissions
        .grant("flee")
        .deny("sing");

### Checking permissions

As with roles, checking permissions is done with the `Identity#isGranted` function.

    identity.isGranted("sing"); // false
    identity.isGranted("flee"); // true
    identity.isGranted("slay rabbit"); // false

## RBAC + ACL

It is possible to combine both approaches and use role-based access along with an access control list. This is achieved by adding roles to an identity.

    identity.addRole(sirRobin);

From this point onwards, the identity will report that it's granted the same permissions as the role `sirRobin`. Since `sirRobin` has subordinate roles, the identity will also inherit permissions from those roles.

## API reference

### Factory

#### `identity()`

Construct a new `Identity` instance

* returns `Identity`

#### `permissions()`

Construct a new `Permissions` instance

* returns `Permissions`

#### `rbac()`

Construct a new `Rbac` instance

* returns `Rbac`

#### `role(roleName)`

Construct a new `Role` instance

* parametre `roleName` (`String`) - name for the role
* returns `Role`

### Rbac

#### `addRole(role, overwrite)`

Add a role to RBAC.

* parametre `role` (`String` or `Role`) - either  role name or a role object
* parametre `overwrite` (`Boolean`) - flag indicating if the role should be overwritten in case  there's already a role with the matching name. Defaults to `false`.
* returns `Rbac` - provides a fluent interface

#### `getRole(roleName)`

Fetch a given role. The role must be added to `Rbac` first.

* parametre `roleName` (`String`) - name of the role to fetch
* returns `Role`

#### `hasRole(roleName)`

Check whether the role with a given name is already defined.

* parametre `roleName` (`String`) - name of the role to fetch
* returns `Boolean`

#### `hydrate(input)`

Hydrate the `Rbac` object using a serialised object.

* parametre `input` (`Object`) - serialised object containing roles and their relationships, as returned by `Rbac#serialise`.
* returns `Rbac` - provides a fluent interface

#### `roles`

Roles defined in the object.

* type `Object`

#### `serialise()`

Serialise the defined roles to a plain object.

* returns `Object`

### Role

#### `addSubordinate(subordinate)`

Add a subordinate role.

* parametre `subordinate` (`Role`) - role that will be the current role's subordinate
* returns `Role` - provides a fluent interface

#### `addSuperior(superior)`

Add a superior role.

* parametre `superior` (`Role`) - role that will be the current role's superior
* returns `Role` - provides a fluent interface

#### `hasSubordinate(role, direct)`

Check if the role has a subordinate role.

* parametre `role` (`Role`|`String`) - either a `Role` object or a string containing the role name
* parametre `direct` (`Boolean`) - flag indicating whether only direct subordinates should be checked (`true`) or all subordinate roles, including subordinates of subordinates (`false`). Defaults to `false`.
* returns `Boolean`

#### `hasSuperior(role, direct)`

Check if the role has a superior role.

* parametre `role` (`Role`|`String`) - either a `Role` object or a string containing the role name
* parametre `direct` (`Boolean`) - flag indicating whether only direct superiors should be checked (`true`) or all superior roles, including superiors of superiors (`false`). Defaults to `false`.
* returns `Boolean`

#### `hydrate(input, rbac)`

Hydrate the role using a serialised object.

* parametre `input` (`Object`) - output of `Role#serialise`.
* parametre `rbac` (`Rbac`) - the `Rbac` object containing the role and its superior/subordinate roles
* returns `Role` - provides a fluent interface

#### `isGranted(permission)`

Check if the role has access to a permission. Subordinate roles are checked if the permission is not found on the role in question.

* parametre `permission` (`String`) - permission name
* returns `Boolean`

#### `name`

The name of the role.

* type `String`

#### `permissions`

Role permissions

* type `Permissions`

#### `serialise()`

Serialise the role to a plain object.

* returns `Object`

#### `subordinates`

Role's subordinate roles.

* type `Object`

#### `superiors`

Role's superior roles.

* type `Object`

### Identity

#### `addRole(role)`

Add a role to the identity.

* parametre `role` (`uAccess.Role`) - role to be added
* returns `Identity` - provides a fluent interface

#### `hasRole(role)`

Check if the identity has a role.

* parametre `roleName` (`String|uAccess.Role`) - the role or role name to check
* returns `Boolean`

#### `hydrate(input, rbac)`

Build the identity based on the serialised output of `Identity#serialise`.

* parametre `input` (`Object`) - output of `Identity#serialise`
* parametre `rbac` (`Rbac`) - an instance of `Rbac` that will provide the roles for the identity
* returns `Identity` - provides a fluent interface

#### `isGranted(permission)`

Check if the identity has access to a permission. The permissions added directly to the identity (ACL) have precedence over permissions added to identity roles (RBAC).

* parametre `permission` (`String`) - name of the permission to check

#### `permissions`

Identity permissions

* type `Permissions`

#### `roles`

Identity roles

* type `Role[]`

#### `serialise()`

Serialise the identity to a plain object

* returns `Object`

#### `setRoles(roles)`

Clear the roles already assigned to the identity and add a new set.

* parametre `roles` (`uAccess.Role[]`) - array of `Role` objects that will become the identity roles
* returns `Identity` - provides a fluent interface

### Permissions

The `Permissions` objects are present in `Role` and `Identity` objects as `Role.permissions` and `Identity.permissions`, respectively.

#### `deny(permission)`

Deny access to a resource

* parametre `permission` (`String`) - permission name
* returns `Permissions` - provides a fluent interface

#### `get(permission)`

Get the value of a given permission

* parametre `permission` (`String`) - permission name
* returns `Boolean`

#### `grant(permission)`

Grant access to a resource

* parametre `permission` (`String`) - permission name
* returns `Permissions` - provides a fluent interface

#### `has(permission)`

Check if a given permission is set

* parametre `permission` (`String`) - permission name
* returns `Boolean`

#### `set(permissions)`

Replace any defined permissions with a new set

* parametre `permissions` (`Object`) - object with permission names as keys and `Boolean` values
* returns `Permissions` - provides a fluent interface
